const exercise = require('./perf');
const urlRegexFactory = require('url-regex');

async function main() {
	const regex = urlRegexFactory({strict: true});
	await exercise((buffer) => {
		const urls = [];
		buffer.toString().replace(regex, (url) => {
			urls.push(url);
			return url;
		});
		return urls;
	});
}

main();