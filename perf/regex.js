const exercise = require('./perf');

async function main() {
	var urlRegex = /(((https?:\/\/)|(www\.))[^\s]+)/g;
	await exercise((buffer) => {
		const urls = [];
		buffer.toString().replace(urlRegex, (url) => {
			urls.push(url);
			return url;
		});
		return urls;
	});
}

main();