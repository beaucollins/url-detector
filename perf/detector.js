const { isSuccess, valueOf } = require('../dist/result');
const { parseAllUrls } = require('../dist/url');
const exercise = require('./perf');

async function main() {
	await exercise(parse);
}

function parse(buffer) {
	const [result, tail] = parseAllUrls(buffer);
	if (!isSuccess(result)) {
		throw new Error('Failed to parse');
	}

	return valueOf(result).reduce(
		(urls, part) => {
			if (part.type === 'url') {
				return [...urls, part.value];
			}
			return urls;
		},
		[]
	);
}

main();