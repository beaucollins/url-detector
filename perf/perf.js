const { createReadStream } = require('fs');
const { join } = require('path');

function read(path) {
	return new Promise((resolve, reject) => {
		const buffers = [];
		createReadStream(path)
			.on('error', reject)
			.on('data', (chunk) => {
				buffers.push(Buffer.from(chunk));
			})
			.on('end', () => {
				resolve(Buffer.concat(buffers));
			});
	});
}


function readSample() {
	return read(join(__dirname, '../__tests__/text/large.txt'));
}

/**
 * Parser takes a Buffer and returns an array of URLs.
 * @param {Function} parser
 */
async function createParser(parser, size=32) {
	const sample = await readSample();
	const buffer = Buffer.concat(new Array(size).fill(sample));
	return () => parser(buffer);
}

function createProgress(threshold = 1000) {
	let last = -Infinity;
	return (iteration, totalTimes) => {
		const now = Date.now();
		if (now - last > threshold) {
			process.stderr.write('\r' + (Math.round(iteration/totalTimes * 100)) + '%');
		}
	};
}

async function exercise(bufferParser, times=1000) {
	const progress = createProgress();
	const parser = await createParser(bufferParser);
	const sample = await createParser(bufferParser, 1)
	console.warn('URLs', sample());
	const start = Date.now();
	for (let i=0; i<times; i++) {
		parser();
		if (i % 20 === 0) progress(i, times);
	}
	process.stderr.write(`\rCount ${times} at ${(Date.now() - start)/times} ms per parse\n`);
}

module.exports = exercise;