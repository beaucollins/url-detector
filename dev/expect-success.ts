import { Result, isSuccess, isFailure, valueOf } from '../src/result';

export function expectSuccess<T>(result: Result<T,any>, value: T) {
	if (isFailure(result)) {
		throw new Error(`Parse failure: ${JSON.stringify(result.reason)}`);
	}
	expect(valueOf(result)).toEqual(value);
}