export type Success<T> = {type: 'success', value: T};
export type Failure<T> = {type: 'failure', reason: T};
export type Result<O,E> = Success<O> | Failure<E>;

export function isSuccess<O>(result: Result<O,any>): result is Success<O> {
	return result.type === 'success';
}

export function isFailure<E>(result: Result<any,E>): result is Failure<E> {
	return result.type === 'failure';
}

export function success<T>(value: T): Success<T> {
	return { type: 'success', value };
}

export function failure<T>(reason: T): Failure<T> {
	return { type: 'failure', reason };
}

export function valueOf<T>(success: Success<T>): T {
	return success.value;
}

export function failureReason<T>(failure: Failure<T>): T {
	return failure.reason;
}
