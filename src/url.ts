import { parseChar, parseMany, parseMaybe, parseFollowedBy, parseOneOf, parseOneOrMore, mapSuccess, Parser, mapResult } from './parse';
import { Result, Success, failure, success, isSuccess, valueOf, failureReason, isFailure } from './result';

type BufferParser<T, E> = Parser<Buffer, T, E>;

export function parseAnyChar(from:string): BufferParser<string, string> {
	const parsers = from.split('').map(parseChar);
	if (parsers.length === 0) {
		throw new Error('No characters provided for parsing.');
	}
	const [head, ...rest] = parsers;
	if (rest.length === 0) {
		return head;
	}
	return parseOneOf(head, ...rest);
}

export function parseNotAnyChar(from: string): BufferParser<string, string> {
	const parsers = from.split('').map(notChar);
	if (parsers.length === 0) {
		throw new Error('No characters provided for parsing.');
	}
	const [head, ...rest] = parsers;
	if (rest.length === 0) {
		return head;
	}

	return function _parseNotAnyChar(input) {

		let [result, tail] = head(input);
		if (isFailure(result)) {
			return [result, tail];
		}

		for(const parser of rest) {
			[result, tail] = parser(input);
			if (isFailure(result)) {
				return [result, tail];
			}
		}
		return [result, tail];
	}
}

const alpha = parseAnyChar('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
const digit = parseAnyChar('0123456789');
const alphaNumeric = parseOneOf(alpha, digit);
/**
  pchar       = unreserved / pct-encoded / sub-delims / ":" / "@"
  pct-encoded = "%" HEXDIG HEXDIG
  unreserved  = ALPHA / DIGIT / "-" / "." / "_" / "~"
  sub-delims  = "!" / "$" / "&" / "'" / "(" / ")"
              / "*" / "+" / "," / ";" / "="
 */
const unreserved = parseOneOf(alphaNumeric, parseAnyChar('-._~/'));
const parseHex = parseAnyChar('0123456789ABCDEFabcdef');
const pctEncoded = mapSuccess(
	parseFollowedBy(parseChar('%'), parseFollowedBy(parseHex, parseHex)),
	([_, [a, b]]): Success<string> => success(`%${a}${b}`)
);

const subDelims = parseAnyChar('!$&\'()*+,;=');
const parsePathChars = parseOneOf(
	pctEncoded,
	subDelims,
	unreserved,
	parseChar(':'),
	parseChar('@'),
);

export const parseProto = mapSuccess(
	parseFollowedBy(
		alpha,
		parseFollowedBy(
			parseMany(alphaNumeric),
			parseChar('://')
		)
	),
	([f, [rest, _]]) => success((f + rest.join('')).concat('://'))
);

const parsePort = mapSuccess(
	parseFollowedBy(
		parseChar(':'),
		parseOneOrMore(digit)
	),
	([_, digit]) => success(digit.join(''))
);

const parseDot = parseChar('.');

function notChar<C extends string>(char: C): Parser<Buffer, string, string> {
	const buffer = Buffer.from(char);

	return function _notChar(input) {
		if (input.length === 0) {
			return [failure('Input is empty'), input];
		}

		const head = nextChar(input);

		if (! buffer.equals(head)) {
			return [success(head.toString()), input.slice(head.length)];
		}
		return [failure('Head is character'), input];
	};
}

export const nonWhiteSpace = parseNotAnyChar("\t\n ._:/\\");

export const parseHostName = mapSuccess(
	parseFollowedBy(
		// abcd
		parseOneOrMore(nonWhiteSpace),
		// .abcd
		mapSuccess(
			parseMany(parseFollowedBy(parseDot, parseOneOrMore(alphaNumeric))),
			(parts) => success(parts.map(([_, part]) => part.join('')))
		),
	),
	([first, rest]) => success([first.join('')].concat(rest))
);

const parseNumericNonLeadingZero = mapSuccess(
	parseFollowedBy(
		parseAnyChar('123456789'),
		parseMany(digit)
	),
	([leading, rest]) => success(`${leading}${rest.join('')}`)
);

const parseIPAddressPart = mapSuccess(
	parseFollowedBy(
		parseDot,
		parseOneOf(
			parseNumericNonLeadingZero,
			parseChar('0')
		)
	),
	([_, part]) => success(part)
);

export const parseIPAddress = mapSuccess(
	parseFollowedBy(
		parseNumericNonLeadingZero,
		parseFollowedBy(
			parseIPAddressPart,
			parseFollowedBy(
				parseIPAddressPart,
				mapSuccess(
					parseFollowedBy(parseDot, parseNumericNonLeadingZero),
					([_, part]) => success(part)
				)
			)
		)
	),
	([one, [two, [three, four]]]): Success<[string, string, string, string]> => success([one, two, three, four])
);

const parseHostParts = mapSuccess(
	parseFollowedBy(
		parseHostName,
		parseMaybe(() => success(''), parsePort)
	),
	([host, port]) => success({host, port})
);

export const parseHost = mapSuccess(
	parseHostParts,
	(host) => success(encodeHost(host))
);

const parseQuery = mapSuccess(
	parseFollowedBy(
		parseChar('?'),
		parseMany(parsePathChars)
	),
	([_, chars]) => success(`?${chars.join('')}`)
)

export const parsePath = mapSuccess(
	parseFollowedBy(
		parseChar('/'),
		parseFollowedBy(
			parseMany(parsePathChars),
			parseMaybe(() => success(''), parseQuery)
		)
	),
	([_, [chars, query]]) => success((['/']).concat(chars, query).join(''))
);

export const parseStrictUrl = mapSuccess(
	parseFollowedBy(
		parseProto,
		parseFollowedBy(
			parseHost,
			parseMaybe(() => success(''), parsePath)
		)
	),
	([proto, [host, path]]) => success(`${proto}${host}${path}`),
);

export const parseLooseUrl = mapSuccess(
	parseFollowedBy(
		parseHostParts,
		parseMaybe(() => success(''), parsePath)
	),
	([host, path]) => validateLooseHost(host, path)
);

function encodeHost({host, port}: {host: string[], port: string}): string {
	return [host.join('.')].concat(port === '' ? '' : `:${port}`).join('');
}

function validateLooseHost({host, port}: {host: string[], port: string}, path: string): Result<Tokens, string> {
	if (host.length === 1 && path === '') {
		if (host[0] !== 'localhost') {
			return success({type: 'plain', value: encodeHost({host, port}).concat(path)});
		}
	}

	if (host[host.length-1].length < 2) {
		return success({type: 'plain', value: encodeHost({host, port}).concat(path)});
	}

	return success({ type: 'url', value: encodeHost({host, port}).concat(path)});
}

const parseWord = parseOneOrMore(parseNotAnyChar(' \t\n\r'));
const parseSpace = parseOneOrMore(parseAnyChar(' \t\n\r'));

export type Tokens =
	| { type: 'url', value: string }
	| { type: 'plain', value: string }

export const parseAllUrls: Parser<Buffer, Tokens[], string> = parseMany(
	parseOneOf(
		mapSuccess(parseStrictUrl, (url): Success<Tokens> => success({type: 'url', value: url})),
		parseLooseUrl,
		mapSuccess(
			parseOneOf(
				parseWord,
				parseSpace
			),
			(chars): Success<Tokens> => success({type: 'plain', value: chars.join('')})
		)
	)
);

function nextChar(input: Buffer): Buffer {
	if (input.length === 0) {
		return input;
	}

	const first = input[0];

	if (first <= 0b01111111) {
		return input.slice(0, 1);
	}

	if (first <= 0b11011111) {
		return input.slice(0, 2);
	}

	if (first <= 0b11101111) {
		return input.slice(0, 3);
	}

	return input.slice(0, 4);
}
