import { Result, success, isFailure, valueOf, isSuccess, failure, failureReason } from './result';

export type Output<I,O,E> = [Result<O,E>, I];
export type Parser<I,O,E> = (incoming: I) => Output<I,O,E>;

export function parseOneOrMore<T,E>(parser: Parser<Buffer,T,E>): Parser<Buffer,T[],E> {
	return function _parseOneOrMore(buffer) {
		let next = buffer;
		const results: T[] = [];
		do {
			const [result, tail] = parser(next);
			if (isFailure(result)) {
				if(results.length === 0) {
					return [result, buffer];
				}
				break;
			}
			next = tail;
			results.push(valueOf(result));
		} while(true);
		return [success(results), next];
	}
}

export function parseMany<O,E>(parser: Parser<Buffer,O,E>): Parser<Buffer,O[],E> {
	return function _parseMany(incoming) {
		const parsed: O[] = [];
		let buffer = incoming;
		do {
			const [result, remaining] = parser(buffer);
			if (isFailure(result)) {
				return [success(parsed), remaining]
			}

			buffer = remaining;
			parsed.push(valueOf(result));
		} while(buffer.length > 0);

		return [success(parsed), buffer];
	}
}

type ParserType<T> = T extends Parser<any, infer U, any> ? U : never;
type FailureType<T> = T extends Parser<any, any, infer U> ? U : never;

export function parseOneOf<I, O, E, V extends Array<Parser<I, any, any>>>(parser: Parser<I, O, E>, ...parsers: V): Parser<I, O | ParserType<V[number]>, E | FailureType<V[number]>> {
	if (parsers.length === 0) {
		return parser;
	}


	if (parsers.length === 1) {
		const last = parsers[0];
		return function _parseOneOf2(input) {
			const [result, tail] = parser(input);
			if (isSuccess(result)) {
				return [result, tail];
			}
			return last(input);
		};
	}

	const last = parsers[parsers.length-1];
	const options = [parser, ...parsers.slice(0, -1)];
	return function _parseOneOfN(buffer) {
		for (const p of options) {
			const [result, tail] = p(buffer);
			if (isSuccess(result)) {
				return [result, tail];
			}
		}
		return last(buffer);
	}
}

export function parseChar<C extends string>(c: C): Parser<Buffer, C, string> {
	const buffer = Buffer.from(c);
	return function _parseChar(input) {
		const head = input.slice(0, buffer.length);
		if (head.equals(buffer)) {
			return [success(c), input.slice(buffer.length)];
		}
		return [failure(`${head} does not match ${c}`), input];
	}
}

export function parseFollowedBy<A,B,E>(a: Parser<Buffer,A,E>, b: Parser<Buffer,B,E>): Parser<Buffer, [A,B], E> {
	return function _parseFollowedBy(buffer) {
		const [resultA, tail] = a(buffer);
		if (isSuccess(resultA)) {
			const [resultB, tailB] = b(tail);
			if (isSuccess(resultB)) {
				return [success([valueOf(resultA), valueOf(resultB)]), tailB];
			} else {
				return [resultB, buffer];
			}
		} else {
			return [resultA, buffer];
		}
	}
}

export function mapSuccess<I, A, B, E>(parser: Parser<I,A,E>, toResult: (from: A) => Result<B,E>): Parser<I,B,E> {
	return mapResult(
		parser,
		result => isSuccess(result) ? toResult(valueOf(result)) : result
	);
}

export function mapFailure<I, O, E, F>(parser: Parser<I,O,E>, fromFailure: (from: E) => Result<O,F>): Parser<I,O,F> {
	return mapResult(
		parser,
		result => isFailure(result) ? fromFailure(failureReason(result)) : result
	)
}

export function mapResult<I, A, B, E, F>(parser: Parser<I,A,E>, toResult: (from: Result<A, E>) => Result<B, F>): Parser<I, B, F> {
	return function _mapResult(input) {
		const [result, tail] = parser(input);

		const mapped = toResult(result);

		if (isSuccess(mapped)) {
			return [mapped, tail];
		}
		return [mapped, input];
	}
}

export function parseMaybe<I,O,E>(whenFailure: () => Result<O,E>, parser: Parser<I,O,E>): Parser<I,O,E> {
	return function _parseMaybe(input) {
		const [result, tail] = parser(input);
		if (isFailure(result)) {
			return [whenFailure(), input];
		}
		return [result, tail];
	}
}

export function parseMultiByte(input: Buffer): Output<Buffer, string, string> {

	if (input.length === 0) {
		return [failure('empty string'), input];
	}

	const first = input[0];

	if (first <= 0b01111111) {
		return [success(input.slice(0, 1).toString()), input.slice(1)];
	}

	if (first <= 0b11011111) {
		return [success(input.slice(0, 2).toString()), input.slice(2)];
	}

	if (first <= 0b11101111) {
		return [success(input.slice(0, 3).toString()), input.slice(3)];
	}

	if (first <= 0b11110111) {
		return [success(input.slice(0, 4).toString()), input.slice(4)];
	}

	throw new Error('not implemented');
}
