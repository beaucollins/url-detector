This is text that we'll be reading into a buffer.

It should have some links in it like www.google.com or even localhost.

http://🚀.example.co.uk should also work.

Something that leads with a dot though like .what.do.you.think shouldn't be identified as a URL.

What if there was weird formatting in here too? Maybe some こにちは.

Or maybe a table formatted with some special characters:

┌─────────────────────────────────────────────────────┐
│                 www.magic.com/💥                    │
└─────────────────────────────────────────────────────┘