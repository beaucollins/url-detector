import { parseMany, parseChar, parseOneOf, parseFollowedBy, parseOneOrMore, mapSuccess, parseMultiByte, Parser } from '../src/parse';
import { isFailure, success } from '../src/result';
import { expectSuccess } from '../dev/expect-success';

describe('buffer', () => {
	it('parseMany', () => {
		const parser = parseMany(parseChar('c'));

		const [result, remaining] = parser(Buffer.from('ccc'));

		expectSuccess(result, ['c','c','c']);
		expect(remaining.toString()).toEqual('');
	});

	describe('parseOneOf cd', () => {
		const parser: Parser<Buffer, string, string> = parseOneOf(parseChar('c'), parseChar('d'));

		it('parses c', () => {
			const [result, remaining] = parser(Buffer.from('cde'));
			expectSuccess(result, "c");
			expect(remaining.length).toBe(2);
		});

		it('parses d', () => {
			const [result, remaining] = parser(Buffer.from('def'))
			expectSuccess(result, 'd');
			expect(remaining.length).toBe(2);
		});

		it('fails to parse e', () => {
			const [result, remaining] = parser(Buffer.from('efg'))
			expect(isFailure(result)).toBe(true);
			expect(remaining.toString()).toEqual('efg');
		})
	});

	describe('parseFollowedBy', () => {
		const parser = parseFollowedBy(parseChar('a'), parseChar('b'));

		it('parser ab', ()=> {
			const [result, tail] = parser(Buffer.from('abc'));
			expectSuccess(result, ["a", "b"]);
			expect(tail.toString()).toEqual('c');
		});

		it('fails acd', () => {
			const [result, tail] = parser(Buffer.from('acd'));
			expect(isFailure(result)).toBe(true);
			expect(tail.toString()).toEqual('acd');
		});
	});

	describe('parseOneOrMore', () => {
		const parser = parseOneOrMore(parseChar('x'));
		it('succeeds x', () => {
			const [result, tail] = parser(Buffer.from('xyz'));
			expectSuccess(result, ['x']);
			expect(tail.toString()).toEqual('yz');
		});

		it('succeeds xxyz', () => {
			const [result, tail] = parser(Buffer.from('xxyz'));
			expectSuccess(result, ['x','x']);
			expect(tail.toString()).toEqual('yz');
		});
	})

	describe('parse multibyte', () => {
		it('parseChar', () => {
			const parser = parseChar('🚀');
			const [result, tail] = parser(Buffer.from('🚀'));

			expectSuccess(result, '🚀');
			expect(tail.length).toBe(0);
		});

		describe('parseMultiByte', () => {
			const parser = mapSuccess(
				parseMany(parseMultiByte),
				(chars) => success(chars.join(''))
			);

			describe('succeeds', () => {
				[
					'💥💩',
					'😃',
					'𐍈',
					'ください',
					'مرحبا',
					'tést',
				].map((chars) => it(chars, () => {
					const [result, tail] = parser(Buffer.from(chars));

					expectSuccess(result, chars);
					expect(tail.toString()).toEqual('');
				}))
			});
		});
	});
});

