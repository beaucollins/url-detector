import { parseProto, parseHost, parsePath, parseStrictUrl, parseIPAddress, parseLooseUrl, nonWhiteSpace, parseNotAnyChar, parseAnyChar, parseAllUrls } from '../src/url';
import { expectSuccess } from '../dev/expect-success';
import { isFailure, failureReason, success, valueOf, isSuccess } from '../src/result';
import { createReadStream } from 'fs';
import { parseMany, parseOneOf, mapSuccess, parseOneOrMore, Parser, Output } from '../src/parse';
import { join } from 'path';

describe('url', () => {

	describe('parseProto', () => {
		it('http://', () => {
			const [result, remaining] = parseProto(Buffer.from('http://something'));
			expectSuccess(result, 'http://');
			expect(remaining.toString()).toEqual('something');
		});
		it('https://', () => {
			const [result, remaining] = parseProto(Buffer.from('https://something'));
			expectSuccess(result, 'https://');
			expect(remaining.toString()).toEqual('something');
		});
		it('fails', () => {
			const [result, remaining] = parseProto(Buffer.from('httpfoo'));
			expect(remaining.toString()).toEqual('httpfoo');
			expect(isFailure(result)).toBe(true);
		});
	});

	describe('parseHost', () => {
		it('bare host', () => {
			const [result, tail] = parseHost(Buffer.from('abcdefg'));
			expectSuccess(result, 'abcdefg');
			expect(tail.toString()).toEqual('');
		});

		it('host without path', () => {
			const [result, tail] = parseHost(Buffer.from('abcdefg/'));
			expectSuccess(result, 'abcdefg');
			expect(tail.toString()).toEqual('/');
		});

		it('host with dots', () => {
			const [result, tail] = parseHost(Buffer.from('a.b.c.d/'));
			expectSuccess(result, 'a.b.c.d');
			expect(tail.toString()).toEqual('/');
		});

		it('host with port', () => {
			const [result, tail] = parseHost(Buffer.from('a.b.c:80865/other'));
			expectSuccess(result, 'a.b.c:80865');
			expect(tail.toString()).toEqual('/other');
		});

		describe('succeeds with', () => {
			[
				'😃.example.com',
				'🚀.example.com',
				'münchen.example.com',
			].map(host => it(host, () => {
				const [result, tail] = parseHost(Buffer.from(host));
				expectSuccess(result, host);
				expect(tail.toString()).toEqual('');
			}));
		});
	});

	describe('parsePath', () => {
		it('parses single wack', () => {
			const [result, tail] = parsePath(Buffer.from('/'));
			expectSuccess(result, '/');
			expect(tail).toEqual(Buffer.from(''));
		});

		it('parses disgusting stuff', () => {
			const ick = '/b/ref=s9_acsd_hfnv_hd_bw_bxiB_ct_x_ct01?node=229668&pf_rd_m=ATVPDKIKX0DER&pf_rd_s=merchandised-search-11&pf_rd_r=738GV6Z0K16V706GEEFY&pf_rd_t=101&pf_rd_p=d6294c39-42f3-5b34-b1c2-c5797fa2c8e5&pf_rd_i=229535';
			const [result, tail] = parsePath(Buffer.from(ick));
			expectSuccess(result, ick);
			expect(tail.toString()).toEqual('');
		});
	})

	describe('parseIPAddress', () => {
		describe('succeeds with', () => {
			[
				'127.0.0.1',
				'10.3.3.1',
				'254.254.254.254'
			].map(ip => it(`parser ${ip}`, () => {
				const [result, tail] = parseIPAddress(Buffer.from(ip));
				expectSuccess(result, ip.split('.'));
				expect(tail.length).toEqual(0);
			}));
		})

		describe('fails with', () => {
			[
				'0.0.0.0',
				'01.0.0.1',
				'127.0.0.0',
			].map(ip => it(`fails ${ip}`, () => {
				const [result, tail] = parseIPAddress(Buffer.from(ip));
				expect(isFailure(result)).toBe(true);
				expect(tail.toString()).toEqual(ip);
			}));
		});
	});

	describe('parseStrictUrl', () => {
		[
			'http://google.com',
			'https://google.co.uk/',
			'blah://127.0.0.1:93920/thgpgcg9%40',
			'blah://293.123.309.930:93920/thgpgcg9%40?something=else'
		].map(url => it(`parses ${url}`, () => {
			const [result, tail] = parseStrictUrl(Buffer.from(url));
			expectSuccess(result, url);
			expect(tail.toString()).toEqual('');
		}));
	});

	describe('parseLooseURL', () => {
		describe('succeeds', () => {
			[
				'google.com',
				'something.blog',
				'one/index.html',
				'localhost',
				'🚀.example.com'
			].map(txt => it(txt, () => {
				const [result, tail] = parseLooseUrl(Buffer.from(txt));
				expectSuccess(result, {type: 'url', value: txt});
				expect(tail.length).toBe(0);
			}))
		});

		describe('succeeds as plain', () => {
			[
				'word',
				'a:100',
				'something:442',
				'a.b',
				'a.b:80',
			].map(txt => it(txt, () => {
				const [result, tail] = parseLooseUrl(Buffer.from(txt));
				expectSuccess(result, {type: 'plain', value: txt});
				expect(tail.toString()).toEqual('');
			}))
		});

		describe('fails', () => {
			[
				{plain: 'one two', parsed: 'one', remaining: ' two'},
				{plain: 'status:success', parsed: 'status', remaining: ':success'},
			].map(({plain, parsed, remaining}) => it(plain, () => {
				const [result, tail] = parseLooseUrl(Buffer.from(plain));
				expectSuccess(result, { type: 'plain', value: parsed});
				expect(tail.toString()).toEqual(remaining);
			}))
		});
	})

	describe('nonWhiteSpace', () => {
		it('parses', () => {
			const [result, tail] = nonWhiteSpace(Buffer.from('a'));
			expectSuccess(result, 'a');
			expect(tail.length).toEqual(0);
		});
	})

	describe('word parser', () => {
		const wordParser = mapSuccess(parseOneOrMore(parseNotAnyChar(' \t\n\r')), chars => success(chars.join('')));
		const spaceParser = mapSuccess(parseOneOrMore(parseAnyChar(' \t\n\r')), chars => success(chars.join('')));

		it('parses a word', () => {
			const [result, tail] = wordParser(Buffer.from('some words'));
			expectSuccess(result, 'some');
			expect(tail.toString()).toEqual(' words');
		});

		it('parses a space', () => {
			const [result, tail] = spaceParser(Buffer.from(' word'));
			expectSuccess(result, ' ');
			expect(tail.toString()).toEqual('word');
		});

		it('parses word or space', () => {
			const parser = parseMany(parseOneOf(wordParser, spaceParser));
			const [result, tail] = parser(Buffer.from('hello world'));

			expectSuccess(result, ['hello', ' ', 'world']);
			expect(tail.toString()).toEqual('');
		});
	});

	describe('text parser', () => {
		const URLS =  [
			'www.google.com',
			'localhost',
			'http://🚀.example.co.uk',
			'www.magic.com/'
		];

		it('parses all urls', async () => {
			const [result, tail] = await parseFile(parseAllUrls, join( __dirname, 'text/sample.txt'));
			if (isFailure(result)) {
				throw new Error('Failed ' + JSON.stringify(failureReason(result)));
			}

			const found: string[] = valueOf(result).reduce(
				(urls: string[], token) => {
					if (token.type === 'url') {
						return [...urls, token.value];
					}
					return urls;
				},
				[],
			);

			expect(found).toEqual(URLS);

			expect(tail.toString()).toEqual('');
		});

		it('parses large text', async () => {
			const [result, tail] = await parseFile(parseAllUrls, join(__dirname, 'text/large.txt'));
			expect(isSuccess(result)).toBe(true);
			expect(tail.length).toBe(0);
		});
	});
});

function read(path: string): Promise<Buffer> {
	return new Promise((resolve, reject) => {
		const stream = createReadStream(path);
		const buffers: Buffer[] = [];

		stream.on('error', reject);
		stream.on('data', (chunk) => {
			buffers.push(Buffer.from(chunk));
		});

		stream.on('end', () => resolve(Buffer.concat(buffers)));
	});

}

async function parseFile<T>(parser: Parser<Buffer, T, string>, path: string): Promise<Output<Buffer, T, string>> {
	return parser(await read(path));
}
